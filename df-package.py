#!/usr/bin/env python

import requests
import json
import os
from os import listdir, path
import argparse
from glob import iglob
from zipfile import ZipFile
from getpass import getpass

zip_path = '.export.zip'
session_token = ""


def patch_api(name, file_path):
    print("Patching {} API".format(name))

    with open(file_path) as file:
        body = file.read()

        body_json = json.loads(body)
        if len(body_json) == 0:
            return

        request = requests.put(
            base_url + name,
            data=body,
            headers={
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-DreamFactory-Session-Token': session_token
            }
        )
        response = request.json()

        if request.status_code != requests.codes.ok:
            print("Failed patching API: " + response['error']['message'])
            exit()


def patch_package():
    for top_entry in listdir(package_path):
        top_entry_path = path.join(package_path, top_entry)

        if path.isdir(top_entry_path):
            for sub_entry in listdir(top_entry_path):
                api_name = "{}/{}".format(top_entry, sub_entry.replace('.json', ''))
                api_file = path.join(top_entry_path, sub_entry)
                patch_api(api_name, api_file)


def import_package():
    with ZipFile(zip_path, 'w') as zip_file:
        for filename in iglob(package_path + '**/*.json', recursive=True):
            relpath = filename[len(package_path):]
            zip_file.write(filename, arcname=relpath)

    importRequest = requests.post(
        base_url + 'system/package',
        files={
            'files': (zip_path, open(zip_path, 'rb'), 'application/zip')
        },
        headers={
            'Accept': 'application/json',
            'X-DreamFactory-Session-Token': session_token
        }
    )
    importResponse = importRequest.json()

    if 200 <= importRequest.status_code < 300:
        print("Imported package.")
        print("Log: {}".format(json.dumps(importResponse['log'], indent='\t', sort_keys=True)))
        patch_package()
    else:
        print("Failed importing package: " + importResponse['error']['message'])
        exit()

    os.remove(zip_path)


def remove_doc(json_obj):
    if isinstance(json_obj, dict):
        json_obj.pop('doc', None)
        for key in json_obj:
            child = json_obj[key]
            remove_doc(child)
    elif isinstance(json_obj, list):
        for child in json_obj:
            remove_doc(child)


def export_package():
    pkgManifest = ''
    with open(manifest_path) as manifestFile:
        pkgManifest = manifestFile.read()

    pkgRequest = requests.post(
        base_url + 'system/package',
        data=pkgManifest,
        headers={
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-DreamFactory-Session-Token': session_token
        }
    )
    pkgResponse = pkgRequest.json()

    if pkgRequest.status_code != requests.codes.ok:
        print("Failed exporting package. " + pkgResponse['error']['message'])
        exit()
    else:
        print("Exported package to " + pkgResponse['path'])

    dlRequest = requests.get(pkgResponse['path'])

    if dlRequest.status_code != requests.codes.ok:
        print("Failed downloading package. " + dlRequest.content.decode('utf-8'))
        exit()
    else:
        with open(zip_path, 'wb') as file:
            file.write(dlRequest.content)

        with ZipFile(zip_path, 'r') as zip_file:
            zip_file.extractall(package_path)

        os.remove(zip_path)

        for filename in iglob(package_path + '**/*.json', recursive=True):
            with open(filename, mode='r+') as file:
                file_json = json.load(file)
                remove_doc(file_json)
                file.seek(0)
                json.dump(file_json, file, indent='\t', sort_keys=True)
                file.truncate()

        print("Extracted package to server/df")


argparser = argparse.ArgumentParser(description="playtecher.com DreamFactory export/import tool")
argparser.add_argument('way', choices=['export', 'import'], help="Import or export")
argparser.add_argument('package_path', help="Directory to export to or import from (usually server/df/)")
argparser.add_argument('--host', help="Host to import/export from", default="http://localhost")

args = argparser.parse_args()
export_path = os.path.join(args.package_path, '')
package_path = os.path.join(os.path.join(export_path, 'df'), '')
manifest_path = os.path.join(export_path, 'df-manifest.json')
command = args.way
base_url = args.host + '/api/v2/'

print("Admin access is required. Please enter your credentials.")
email = input("Email: ")
password = getpass("Password: ")

loginRequest = requests.post(
    base_url + 'system/admin/session',
    data={
        'email': email,
        'password': password,
        'duration': 0
    },
    headers={
        'Accept': 'application/json'
    }
)

if loginRequest.status_code != requests.codes.ok:
    try:
        response = loginRequest.json()
        print("Failed logging in. " + response['error']['message'])
    except json.decoder.JSONDecodeError:
        response = loginRequest.text
        print("Failed logging in. " + response)

    exit()
else:
    print("Logged in.")
    response = loginRequest.json()
    session_token = response['session_token']

if command == 'export':
    export_package()
elif command == 'import':
    import_package()
else:
    print("Command is invalid: {}".format(command))
